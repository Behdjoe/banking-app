var activePage = '', UI = '{}', BA = '{}';

// serialization function to turn form data into json object
$.fn.serializeObject = function(){
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};

//submit the account data
function submitForm(data){
    event.preventDefault();
    var obj = $(data).serializeObject();
    var objStr = JSON.stringify(obj);
    localStorage.setItem(data.id, objStr);
}

//set the form data, using local storage
function setFormInfo(data){
    if (data == '{}' || data == null || !data){
        return false;
    }
    else {
        var obj = JSON.parse(data);
        $.each(obj, function (key, val) {
            var selector = "input#" + key;
            $(selector).val(val);
        });
    }
}

//make payment
function makePayment(data){
    event.preventDefault();
    var obj = $(data).serializeObject();
    var objStr = JSON.stringify(obj);
    localStorage.setItem(data.id, objStr);
    console.error(obj, objStr);
}

//render accounts
function renderAccounts() {
    $.getJSON( "js/bankAccounts.json", function( data ) {
        var items = [], accounts = [];
        var objStr = JSON.stringify(data.accounts);
        localStorage.setItem('bankAccounts', objStr);

        $.each( data.accounts, function( key, val ) {
            accounts.push("<option value='"+val.number+"'>"+ val.number +"</option>");

            items.push(
                "<li class='bank-account'>"+
                "<p class='account-name'>" + val.name + "</p>" +
                "<span class='account-number'>" + val.number + "</span>" +
                "<div class='account-balance' >" +
                    "<span class='euro'>€ </span>" +
                    "<span class='amount'>" + val.balance + ", </span>" +
                    "<span class='fractions'>" + val.fractions + "</span>" +
                "</div>" +
                "</li>"
            );
        });
        $("<ul/>", {
            "class": "bank-accounts",
            html: items.join("")
        }).appendTo(".bankaccounts");

        $("<select/>", {
            "class": "selectBankAccounts",
            "name": "fromAccount",
            html: accounts.join("")
        }).appendTo(".listAccounts");
    });
}

//render transactions
function renderTransactions() {
    $.getJSON( "js/transactions.json", function( data ) {
        var items = [];
        var objStr = JSON.stringify(data.transactions);
        localStorage.setItem('transactions', objStr);

        $.each( data.transactions, function( key, val ) {
            items.push(
                "<li class=''>"+
                    "<p class='name'>" + val.userName + "</p>" +
                    "<span class='date'>" + val.date + "</span>" +
                    "<div class=''>" +
                        "<span class='type "+val.type+"'>" + val.type + "</span>" +
                        "<span class='amount'>" + val.amount + "</span>" +
                    "</div>" +
                "</li>"
            );
        });
        $("<ul/>", {
            "class": "transactionList",
            html: items.join("")
        }).appendTo(".myTransactions");
    });
}

//--------- ------ ---- on page load
$(document).ready(function () {
    console.clear();

    //get Local storage info
    UI = localStorage.getItem('userInfo');
    BA = localStorage.getItem('bankAccounts');

    // hash page mechanic
    function loadPage(hashname) {
        activePage = hashname || 'home';
        var navTag = activePage.replace('#', '');

        //load the correct page
        $('#app').load('pages/'+ navTag +'.html', function(){
            setFormInfo(UI);
            renderAccounts();
            renderTransactions();
        });

        //set the navigation active tag
        $('.nav li a').removeClass('active');
        $('a.' + navTag).addClass('active');
    }

    $(window).bind('hashchange', function () {
        // Show the page on hash change
        loadPage(window.location.hash.toString());

        //set the user information on page load
        // setFormInfo(UI);
    });

    // Show the correct page on load
    loadPage(window.location.hash.toString());
});
