#banking app by Behdjoe Gholami
##created using the HTML5 Boilerplate

## Prerequisites
* Node (optional)
* Git (optional)

## Quick start

* download or clone the repo using git
```
#!node

git clone git@bitbucket.org:Behdjoe/banking-app.git
```

* use node to install the modules (optional)
```
#!node

npm install 
```

* open the project
```
#!html
open index.html in the 'src' folder to see the project
```

## screenshots

![Screen Shot 2016-07-22 at 11.30.48.png](https://bitbucket.org/repo/nLAnak/images/1127499806-Screen%20Shot%202016-07-22%20at%2011.30.48.png)

![Screen Shot 2016-07-22 at 11.32.58.png](https://bitbucket.org/repo/nLAnak/images/132298187-Screen%20Shot%202016-07-22%20at%2011.32.58.png)

![Screen Shot 2016-07-22 at 11.57.49.png](https://bitbucket.org/repo/nLAnak/images/2716863939-Screen%20Shot%202016-07-22%20at%2011.57.49.png)

## Features

* HTML5 ready. Use the new elements with confidence.
* Designed with progressive enhancement in mind.
* Includes:
  * [`Normalize.css`](https://necolas.github.com/normalize.css/)
    for CSS normalizations and common bug fixes
  * [`jQuery`](https://jquery.com/) via CDN, with a local fallback
  * A custom build of  [`Modernizr`](http://modernizr.com/) for feature
    detection
  * [`Apache Server Configs`](https://github.com/h5bp/server-configs-apache)
    that, among other, improve the web site's performance and security
* Placeholder CSS Media Queries.
* Useful CSS helper classes.
* Default print styles, performance optimized.
* An optimized version of the Google Universal Analytics snippet.
* Protection against any stray `console` statements causing JavaScript
  errors in older browsers.
* "Delete-key friendly." Easy to strip out parts you don't need.
* Extensive inline and accompanying documentation.


## Browser support

* Chrome *(latest 2)*
* Edge *(latest 2)*
* Firefox *(latest 2)*
* Internet Explorer 8+
* Opera *(latest 2)*
* Safari *(latest 2)*

*This doesn't mean that HTML5 Boilerplate cannot be used in older browsers,
just that we'll ensure compatibility with the ones mentioned above.*

If you need legacy browser support (IE 6+, Firefox 3.6+, Safari 4+) you
can use [HTML5 Boilerplate v4](https://github.com/h5bp/html5-boilerplate/tree/v4),
but is no longer actively developed.


## Documentation

Take a look at the [documentation table of contents](dist/doc/TOC.md).
This documentation is bundled with the project, which makes it readily
available for offline reading and provides a useful starting point for
any documentation you want to write about your project.


## Contributing

Hundreds of developers have helped make the HTML5 Boilerplate what it is
today. Anyone and everyone is welcome to [contribute](CONTRIBUTING.md),
however, if you decide to get involved, please take a moment to review
the [guidelines](CONTRIBUTING.md):

* [Bug reports](CONTRIBUTING.md#bugs)
* [Feature requests](CONTRIBUTING.md#features)
* [Pull requests](CONTRIBUTING.md#pull-requests)


## License

The code is available under the NINJA STRIJDER license.